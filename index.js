/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};

/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {

/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId])
/******/ 			return installedModules[moduleId].exports;

/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			exports: {},
/******/ 			id: moduleId,
/******/ 			loaded: false
/******/ 		};

/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);

/******/ 		// Flag the module as loaded
/******/ 		module.loaded = true;

/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}


/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;

/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;

/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";

/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(0);
/******/ })
/************************************************************************/
/******/ ([
/* 0 */
/***/ function(module, exports) {

	'use strict';

	var canvas = document.getElementById('canvas');

	var width = window.innerWidth;

	var height = window.innerHeight;

	var aspect = width / height;

	var renderer = new THREE.WebGLRenderer({ canvas: canvas, antialias: true });

	renderer.setSize(width, height);

	var camera = new THREE.PerspectiveCamera(45, aspect);

	camera.position.set(100, 100, 100);

	var controls = new THREE.OrbitControls(camera);

	var scene = new THREE.Scene();

	var loader = new THREE.OBJLoader();

	scene.add(new THREE.HemisphereLight(0xf2f2f2, 0xafafaf, 1));

	var light = new THREE.PointLight(0xf2f2f2, 0.4);

	light.position.set(250, 120, 210);

	scene.add(light);

	light = new THREE.PointLight(0xf2f2f2, 0.4);

	light.position.set(-200, 150, 112);

	scene.add(light);

	light = new THREE.PointLight(0xf2f2f2, 0.3);

	light.position.set(0, -230, 0);

	scene.add(light);

	loader.load('/teeth.obj', function (o) {

	    console.log("OBJ", o);

	    o.traverse(function (m) {

	        console.log(m);

	        if (m instanceof THREE.Mesh) {
	            m.material = new THREE.MeshStandardMaterial({ color: 0x92aedb, metalness: 0.2, roughness: 0.6 });
	        }
	    });

	    scene.add(o);
	});

	var render = function render() {
	    requestAnimationFrame(render);

	    renderer.render(scene, camera);
	};

	window.onresize = function () {

	    var newWidth = window.innerWidth;
	    var newHeight = window.innerHeight;

	    var newAspect = newWidth / newHeight;

	    renderer.setSize(newWidth, newHeight);

	    camera.aspect = newAspect;

	    camera.updateProjectionMatrix();
	};

	render();

/***/ }
/******/ ]);