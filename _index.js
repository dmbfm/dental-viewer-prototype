let canvas = document.getElementById('canvas');

let width = window.innerWidth;

let height = window.innerHeight;

let aspect = width / height;

let renderer = new THREE.WebGLRenderer({canvas: canvas, antialias: true});

renderer.setSize(width, height);

let camera = new THREE.PerspectiveCamera(45, aspect);

camera.position.set(100, 100, 100);

let controls = new THREE.OrbitControls(camera);

let scene = new THREE.Scene();

let loader = new THREE.OBJLoader();

scene.add(new THREE.HemisphereLight( 0xf2f2f2, 0xafafaf, 1));

let light = new THREE.PointLight(0xf2f2f2, 0.4);

light.position.set(250, 120, 210);

scene.add(light);

light = new THREE.PointLight(0xf2f2f2, 0.4);

light.position.set(-200, 150, 112);

scene.add(light);

light = new THREE.PointLight(0xf2f2f2, 0.3);

light.position.set(0, -230, 0);

scene.add(light);

loader.load('/teeth.obj', o => {

    console.log("OBJ", o);

    o.traverse( m => {

        console.log(m);

        if (m instanceof THREE.Mesh) {
            m.material = new THREE.MeshStandardMaterial({color: 0x92aedb, metalness: 0.2, roughness: 0.6});
        }

    });

    scene.add(o);

});


let render = () => {
    requestAnimationFrame(render);

    renderer.render(scene, camera);

}

window.onresize = () => {

    let newWidth = window.innerWidth;
    let newHeight = window.innerHeight;

    let newAspect = newWidth / newHeight;

    renderer.setSize(newWidth, newHeight);

    camera.aspect = newAspect;

    camera.updateProjectionMatrix();

}

render();




